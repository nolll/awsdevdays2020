{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Direct Marketing with Amazon SageMaker XGBoost with Hyperparameter Tuning\n",
    "\n",
    "Last update: February 6th, 2020"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%sh\n",
    "pip -q install --upgrade pip\n",
    "pip -q install sagemaker awscli boto3 smdebug --upgrade"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.core.display import HTML\n",
    "HTML(\"<script>Jupyter.notebook.kernel.restart()</script>\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Downloading and processing the dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start by downloading the [direct marketing dataset](https://archive.ics.uci.edu/ml/datasets/bank+marketing) from UCI's ML Repository."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!wget -N https://archive.ics.uci.edu/ml/machine-learning-databases/00222/bank-additional.zip\n",
    "!unzip -o bank-additional.zip"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!head ./bank-additional/bank-additional-full.csv"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need to load this CSV file, inspect it, pre-process it, etc. Please don't write custom Python code to do this!\n",
    "\n",
    "Instead, developers typically use libraries such as:\n",
    "* Pandas: a library providing high-performance, easy-to-use data structures and data analysis tools for the Python programming language: https://pandas.pydata.org/.\n",
    "* Numpy: a fundamental package for scientific computing with Python: http://www.numpy.org/\n",
    "\n",
    "Along the way, we'll use functions from these two libraries. You should definitely become familiar with them, they will make your life much easier when working with large datasets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np  # For matrix operations and numerical processing\n",
    "import pandas as pd # For munging tabular data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's read the CSV file into a Pandas data frame and take a look at the first few lines."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# https://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_csv.html\n",
    "data = pd.read_csv('./bank-additional/bank-additional-full.csv', sep=';')\n",
    "pd.set_option('display.max_columns', 500)     # Make sure we can see all of the columns\n",
    "pd.set_option('display.max_rows', 50)         # Keep the output on one page\n",
    "data[:10] # Show the first 10 lines"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.shape # (number of lines, number of columns)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The two classes are extremely unbalanced and it could be a problem for our classifier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "one_class = data[data['y']=='yes']\n",
    "one_class_count = one_class.shape[0]\n",
    "print(\"Positive samples: %d\" % one_class_count)\n",
    "\n",
    "zero_class = data[data['y']=='no']\n",
    "zero_class_count = zero_class.shape[0]\n",
    "print(\"Negative samples: %d\" % zero_class_count)\n",
    "\n",
    "zero_to_one_ratio = zero_class_count/one_class_count\n",
    "print(\"Ratio: %.2f\" % zero_to_one_ratio)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Transforming the dataset\n",
    "Cleaning up data is part of nearly every machine learning project.  It arguably presents the biggest risk if done incorrectly and is one of the more subjective aspects in the process."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First of all, many records have the value of \"999\" for pdays, number of days that passed by after a client was last contacted. It is very likely to be a magic number to represent that no contact was made before. Considering that, we create a new column called \"no_previous_contact\", then grant it value of \"1\" when pdays is 999 and \"0\" otherwise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "[np.min(data['pdays']), np.max(data['pdays'])]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Indicator variable to capture when pdays takes a value of 999\n",
    "# https://docs.scipy.org/doc/numpy-1.15.1/reference/generated/numpy.where.html\n",
    "data['no_previous_contact'] = np.where(data['pdays'] == 999, 1, 0)\n",
    "data = data.drop(['pdays'], axis=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the \"job\" column, there are categories that mean the customer is not working, e.g., \"student\", \"retire\", and \"unemployed\". Since it is very likely whether or not a customer is working will affect his/her decision to enroll in the term deposit, we generate a new column to show whether the customer is working based on \"job\" column."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data['job'].value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Indicator for individuals not actively employed\n",
    "# https://docs.scipy.org/doc/numpy-1.15.0/reference/generated/numpy.in1d.html\n",
    "data['not_working'] = np.where(np.in1d(data['job'], ['student', 'retired', 'unemployed']), 1, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Last but not the least, we convert categorical to numeric, as is suggested above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# https://pandas.pydata.org/pandas-docs/stable/generated/pandas.get_dummies.html\n",
    "model_data = pd.get_dummies(data)  # Convert categorical variables to sets of indicators\n",
    "model_data[:10]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, each categorical column (job, marital, education, etc.) has been replaced by a set of new columns, one for each possible value in the category. Accordingly, we now have 67 columns instead of 21."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_data.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Splitting the dataset\n",
    "\n",
    "We'll then split the dataset into training (70%), validation (20%), and test (10%) datasets and convert the datasets to the right format the algorithm expects. We will use training and validation datasets during training and we will try to maximize the accuracy on the validation dataset.\n",
    " \n",
    "Once the model has been deployed, we'll use the test dataset to evaluate its performance.\n",
    "\n",
    "Amazon SageMaker's XGBoost algorithm expects data in the libSVM or CSV data format.  For this example, we'll stick to CSV.  Note that the first column must be the target variable and the CSV should not include headers.  Also, notice that although repetitive it's easiest to do this after the train|validation|test split rather than before.  This avoids any misalignment issues due to random reordering."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the seed to 123 for reproductibility\n",
    "# https://pandas.pydata.org/pandas-docs/version/0.22/generated/pandas.DataFrame.sample.html\n",
    "# https://docs.scipy.org/doc/numpy-1.15.1/reference/generated/numpy.split.html\n",
    "train_data, validation_data, test_data = np.split(model_data.sample(frac=1, random_state=123), \n",
    "                                                  [int(0.7 * len(model_data)), int(0.9*len(model_data))])  \n",
    "\n",
    "# Drop the two columns for 'yes' and 'no' and add 'yes' back as first column of the dataframe\n",
    "# https://pandas.pydata.org/pandas-docs/stable/generated/pandas.concat.html\n",
    "pd.concat([train_data['y_yes'], train_data.drop(['y_no', 'y_yes'], axis=1)], axis=1).to_csv('train.csv', index=False, header=False)\n",
    "pd.concat([validation_data['y_yes'], validation_data.drop(['y_no', 'y_yes'], axis=1)], axis=1).to_csv('validation.csv', index=False, header=False)\n",
    "#pd.concat([test_data['y_yes'], test_data.drop(['y_no', 'y_yes'], axis=1)], axis=1).to_csv('test.csv', index=False, header=False)\n",
    "\n",
    "# Dropping the target value, as we will use this CSV file for batch transform\n",
    "test_data.drop(['y_no', 'y_yes'], axis=1).to_csv('test.csv', index=False, header=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls -l *.csv"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we'll copy the files to S3 for Amazon SageMaker training to pickup."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sagemaker\n",
    "import boto3, os\n",
    "\n",
    "bucket = sagemaker.Session().default_bucket()                     \n",
    "prefix = 'sagemaker/DEMO-xgboost-dm'\n",
    "\n",
    "boto3.Session().resource('s3').Bucket(bucket).Object(os.path.join(prefix, 'train/train.csv')).upload_file('train.csv')\n",
    "boto3.Session().resource('s3').Bucket(bucket).Object(os.path.join(prefix, 'validation/validation.csv')).upload_file('validation.csv')\n",
    "boto3.Session().resource('s3').Bucket(bucket).Object(os.path.join(prefix, 'test/test.csv')).upload_file('test.csv')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "SageMaker needs to know where the training and validation sets are located, so let's define that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s3_input_train = sagemaker.s3_input(s3_data='s3://{}/{}/train'.format(bucket, prefix), content_type='csv')\n",
    "s3_input_validation = sagemaker.s3_input(s3_data='s3://{}/{}/validation/'.format(bucket, prefix), content_type='csv')\n",
    "s3_data = {'train': s3_input_train, 'validation': s3_input_validation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Launching an Automatic Model Tuning job"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The problem we're trying to solve is a classification problem: will a given customer react positively to our marketing offer or not? In order to answer this question, let's train a classification model with XGBoost, a popular open source project available in SageMaker."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.amazon.amazon_estimator import get_image_uri\n",
    "\n",
    "from sagemaker.estimator import Estimator\n",
    "# https://sagemaker.readthedocs.io/en/stable/estimators.html\n",
    "\n",
    "sess = sagemaker.Session()\n",
    "region = boto3.Session().region_name    \n",
    "\n",
    "container = get_image_uri(region, 'xgboost', repo_version='0.90-2')\n",
    "\n",
    "xgb = Estimator(\n",
    "    \n",
    "    container,                                               # The algorithm (XGBoost)\n",
    "    role=sagemaker.get_execution_role(),                     # IAM permissions for SageMaker\n",
    "    sagemaker_session=sess,                                  # Technical object\n",
    "                                    \n",
    "    input_mode='File',                                       # Copy the dataset and then train\n",
    "    output_path='s3://{}/{}/output'.format(bucket, prefix),  # Save the model here\n",
    "                                    \n",
    "    train_instance_count=1,                                  # Instance requirements\n",
    "    train_instance_type='ml.m4.2xlarge',\n",
    "                                    \n",
    "    train_use_spot_instances=True,                           # Use a spot instance\n",
    "    train_max_run=300,                                       # Max training time\n",
    "    train_max_wait=600,                                      # Max training time + spot waiting time\n",
    ")\n",
    "\n",
    "xgb.set_hyperparameters(objective='binary:logistic', \n",
    "                        num_round=100,\n",
    "                        early_stopping_rounds=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use SageMaker tuning to automate the searching process effectively. Specifically, we specify a range, or a list of possible values in the case of categorical hyperparameters, for each of the hyperparameter that we plan to tune. SageMaker hyperparameter tuning will automatically launch multiple training jobs with different hyperparameter settings, evaluate results of those training jobs based on a predefined \"objective metric\", and select the hyperparameter settings for future attempts based on previous results. For each hyperparameter tuning job, we will give it a budget (max number of training jobs) and it will complete once that many training jobs have been executed.\n",
    "\n",
    "We will tune four hyperparameters in this example. Don't worry if this sounds over-complicated, we don't need to understand this in detail right now."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.tuner import IntegerParameter, ContinuousParameter\n",
    "# https://sagemaker.readthedocs.io/en/stable/tuner.html\n",
    "\n",
    "hyperparameter_ranges = {\n",
    "    'eta': ContinuousParameter(0, 1),\n",
    "    'min_child_weight': ContinuousParameter(1, 10),\n",
    "    'alpha': ContinuousParameter(0, 2),\n",
    "    'max_depth': IntegerParameter(1, 10)\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we'll specify the objective metric that we'd like to tune for and its definition. Several metrics are [available](https://docs.aws.amazon.com/sagemaker/latest/dg/xgboost-tuning.html).\n",
    "\n",
    "As our dataset is very unbalanced, accuracy may not be the best metric here: a dumb model predicting zero all the time would be right 80%+ of the time!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "objective_metric_name = 'validation:auc'\n",
    "objective_type = 'Maximize'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we'll create a `HyperparameterTuner` object, to which we pass:\n",
    "- The XGBoost estimator we created above\n",
    "- Our hyperparameter ranges\n",
    "- Objective metric name and definition\n",
    "- Tuning resource configurations such as Number of training jobs to run in total and how many training jobs can be run in parallel."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.tuner import HyperparameterTuner\n",
    "# https://sagemaker.readthedocs.io/en/stable/tuner.html\n",
    "\n",
    "tuner = HyperparameterTuner(\n",
    "    xgb,\n",
    "    objective_metric_name,\n",
    "    hyperparameter_ranges,\n",
    "    objective_type=objective_type,\n",
    "    max_jobs=40,\n",
    "    max_parallel_jobs=4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We're all set. Let's train!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tuner.fit(s3_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's check the status of our debug job."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inspecting jobs with Amazon SageMaker Experiments\n",
    "Model tuning automatically creates a new experiment, and pushes information for each job. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.analytics import HyperparameterTuningJobAnalytics\n",
    "\n",
    "exp = HyperparameterTuningJobAnalytics(\n",
    "    sagemaker_session=sess, \n",
    "    hyperparameter_tuning_job_name=tuner.latest_tuning_job.name\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = exp.dataframe()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.sort_values('FinalObjectiveValue', ascending=0)[:1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deploying the best model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's deploy our model to an HTTPS endpoint, and enable data capture. All it takes is one line of code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from time import strftime, gmtime\n",
    "timestamp = strftime('%d-%H-%M-%S', gmtime())\n",
    "\n",
    "xgb_endpoint = tuner.deploy(\n",
    "    endpoint_name = 'DEMO-xgboost-dm-{}'.format(timestamp),\n",
    "    initial_instance_count = 1,                    # Infrastructure requirements\n",
    "    instance_type = 'ml.m4.xlarge'\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Predicting with our model\n",
    "\n",
    "First we'll need to determine how we pass data into and receive data from our endpoint. Our data is currently stored as NumPy arrays in memory of our notebook instance. To send it in an HTTP POST request, we'll serialize it as a CSV string and then decode the resulting CSV.\n",
    "\n",
    "Let's predict the first 10 samples from the test set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sm = boto3.Session().client(service_name='runtime.sagemaker') \n",
    "\n",
    "test_samples = [line.rstrip('\\n') for line in open('test.csv')]\n",
    "test_samples = test_samples[:100] # We'll predict the first 100 samples\n",
    "\n",
    "for sample in test_samples:\n",
    "    sample = bytes(sample, 'utf-8')\n",
    "    response = sm.invoke_endpoint(EndpointName=xgb_endpoint.endpoint, \n",
    "                                  ContentType='text/csv', \n",
    "                                  Body=sample)\n",
    "    print(response['Body'].read())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deleting the endpoint\n",
    "Once that we're done predicting, we can delete the endpoint (and stop paying for it). You can re-deploy again by running the appropriate cell above. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sagemaker.Session().delete_endpoint(xgb_endpoint.endpoint)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda_python3",
   "language": "python",
   "name": "conda_python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  },
  "notice": "Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.  Licensed under the Apache License, Version 2.0 (the \"License\"). You may not use this file except in compliance with the License. A copy of the License is located at http://aws.amazon.com/apache2.0/ or in the \"license\" file accompanying this file. This file is distributed on an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License."
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
